import { CounterComponent } from './counter.component.jsx';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
	count: state.clickCount
});

export const ConnectedClickCounterComponent = connect(mapStateToProps)(CounterComponent);