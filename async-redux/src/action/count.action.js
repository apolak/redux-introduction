import * as Action from './types';

export const incrementCounter = () => dispatch => {
	fetch('http://deelay.me/1000/http://www.mocky.io/v2/5863bc1a1000002404b48952',{ mode: 'no-cors'})
		.then(response => dispatch({
			type: Action.INCREMENT
		}))
		.catch((err) => console.log(err));
};

export const decrementCounter = () => dispatch => {
	fetch('http://deelay.me/2000/http://www.mocky.io/v2/5863bc1a1000002404b48952', {mode: 'no-cors'})
		.then(response => dispatch({
			type: Action.DECREMENT
		}))
		.catch((err) => console.log(err));
};