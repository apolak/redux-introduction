import React from 'react';

export class ButtonComponent extends React.Component {
	render() {
		return (
			<button onClick={this.props.onClick}>{this.props.label}</button>
		);
	}
}

ButtonComponent.propTypes = {
	label: React.PropTypes.string,
	onClick: React.PropTypes.func
};
