import * as Action from '../action/types';

export const otherMiddleware = store => next => action => {
	switch (action.type) {
		case Action.LOGGED:
			console.log('Other: ', action);

			return next(action);
		default:
			return next(action);
	}
};

