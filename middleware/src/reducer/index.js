import { countReducer } from './count.reducer';
import { clickReducer } from './click.reducer';
import { combineReducers } from 'redux';

export const reducers = combineReducers({
	count: countReducer,
	clickCount: clickReducer
});