import * as Action from '../action/types';

export const countReducer = (state = 0, action) => {
	switch (action.type) {
		case Action.INCREMENT:
			return state + 1;
		case Action.DECREMENT:
			return state - 1;
		default:
			return state;
	}
};