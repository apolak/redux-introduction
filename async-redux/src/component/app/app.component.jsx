import React from 'react';
import { ButtonComponent } from '../button/button.component.jsx';
import { ConnectedClickCounterComponent } from '../counter/connected-click-counter.component';
import { ConnectedCounterComponent } from '../counter/connected-counter.component';

export class AppComponent extends React.Component {
	render() {
		return (
			<div>
				<ConnectedClickCounterComponent label="Click counter:"/>
				<ConnectedCounterComponent label="Counter:"/>
				<ButtonComponent label="Increment" onClick={this.props.increment.bind(this)}/>
				<ButtonComponent label="Decrement" onClick={this.props.decrement.bind(this)}/>
			</div>
		);
	}
}

AppComponent.propTypes = {
	increment: React.PropTypes.func,
	decrement: React.PropTypes.func
};
