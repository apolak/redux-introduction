import * as Action from './types';

export const incrementCounter = () => ({
	type: Action.INCREMENT
});

export const decrementCounter = () => ({
	type: Action.DECREMENT
});