import React from 'react';
import ReactDom from 'react-dom';

import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { createStore, applyMiddleware } from 'redux';
import { reducers } from './reducer';
import { Provider } from 'react-redux';
import { ConnectedAppComponent } from './component/app/connected-app.component';
import thunk from 'redux-thunk';

const initialState = {
	count: 0,
	clickCount: 0
};

const store = createStore(
	reducers,
	initialState,
	composeWithDevTools(
		applyMiddleware(thunk)
	)
);

ReactDom.render(
	<Provider store={store}>
		<ConnectedAppComponent test="123"/>
	</Provider>,
	document.getElementById('root')
);