import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { createStore } from 'redux';
import { reducers } from './reducer';
import * as CountAction from './action/count.action';

const initialState = {
	count: 0,
	clickCount: 0
};

const store = createStore(
	reducers,
	initialState,
	composeWithDevTools()
);

const counter = document.getElementById('counter');
counter.innerHTML = store.getState().count;

const clickCounter = document.getElementById('click-counter');
clickCounter.innerHTML = store.getState().clickCount;

store.subscribe(() => {
	counter.innerHTML = store.getState().count;
	clickCounter.innerHTML = store.getState().clickCount;
	//console.log(store.getState())
});

const dispatchIncrementAction = () => {
	store.dispatch(CountAction.incrementCounter())
};

const dispatchDecrementAction = () => {
	store.dispatch(CountAction.decrementCounter())
};

const incrementButton = document.getElementById('increment-button');
incrementButton.addEventListener('click', () => dispatchIncrementAction());

const decrementButton = document.getElementById('decrement-button');
decrementButton.addEventListener('click', () => dispatchDecrementAction());