import { AppComponent } from './app.component.jsx';
import { connect } from 'react-redux';
import  * as Action from '../../action/count.action';

const mapDispatchToProps = (dispatch) => ({
	increment: () => dispatch(Action.incrementCounter()),
	decrement: () => dispatch(Action.decrementCounter())
});

export const ConnectedAppComponent = connect(null, mapDispatchToProps)(AppComponent);