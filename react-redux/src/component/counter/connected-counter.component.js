import { CounterComponent } from './counter.component.jsx';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
	count: state.count
});

export const ConnectedCounterComponent = connect(mapStateToProps)(CounterComponent);