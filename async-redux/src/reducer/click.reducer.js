import * as Action from '../action/types';

export const clickReducer = (state = 0, action) => {
	switch (action.type) {
		case Action.INCREMENT:
		case Action.DECREMENT:
			return state + 1;
		default:
			return state;
	}
};