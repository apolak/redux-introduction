import React from 'react';

export class CounterComponent extends React.Component {
	render() {
		return (
			<div>
				<p>{ this.props.label } { this.props.count }</p>
			</div>
		);
	}
}

CounterComponent.propTypes = {
	label: React.PropTypes.string,
	count: React.PropTypes.number
};
