import React from 'react';
import ReactDom from 'react-dom';

import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { createStore } from 'redux';
import { reducers } from './reducer';
import { Provider } from 'react-redux';
import { ConnectedAppComponent } from './component/app/connected-app.component';

const initialState = {
	count: 0,
	clickCount: 0
};

const store = createStore(
	reducers,
	initialState,
	composeWithDevTools()
);

ReactDom.render(
	<Provider store={store}>
		<ConnectedAppComponent test="123"/>
	</Provider>,
	document.getElementById('root')
);